import sys, math, random, heapq
import matplotlib.pyplot as plt
from itertools import chain


class Graph:

    def __init__(self, vertices):
        self.vertices = vertices
        self.n = len(vertices)

    def x(self, v):
        return self.vertices[v][0]

    def y(self, v):
        return self.vertices[v][1]

    _d_lookup = {}

    def d(self, u, v):

        # check if already in _d_lookup
        if (u, v) in self._d_lookup:
            return self._d_lookup[(u, v)]

        # compute
        _distance = math.sqrt((u[0] - v[0])**2 + (u[1] - v[1])**2)

        # add to lookup
        self._d_lookup[(u, v)], self._d_lookup[(v, u)] = _distance, _distance

        return _distance

    def plot(self, tour=None):
        """ Plot the cities """
        if tour is None:
            tour = Tour(self, [])

        _vertices = [self.vertices[0]]

        for i in tour.vertices:
            _vertices.append(self.vertices[i])

        plt.figure(figsize=(18, 18))
        plt.title('Cost = ', str(tour.cost()))
        plt.plot(*zip(*_vertices), '-r')
        plt.scatter(*zip(*self.vertices), c='b', s='5')
        plt.show()


class Tour:

    def __init__(self, g, vertices=None):
        """ Generate random tour in given graph g """
        self.g = g

        if vertices is None:
            self.vertices = list(range(1, g.n))
            random.shuffle(self.vertices)
        else:
            self.vertices = vertices

        self.__cost = None

    def cost(self):
        """ Return total edge count on tour """

        if self.__cost is None:
            self.__cost = 0
            for i, j in zip([0] + self.vertices, self.vertices + [0]):
                self.__cost += self.g.d(self.g.vertices[i], self.g.vertices[j])
        return self.__cost


class GeneticAlgorithm:

    def __init__(self, g, population_size, k=5, elite_mating_rate=0.5,
                 mutation_rate=0.015, mutation_swap_rate=0.2):
        """ Init params """
        self.g = g

        self.population = []
        for _ in range(population_size):
            self.population.append(Tour(g))

        self.population_size = population_size
        self.k = k
        self.elite_mating_rate = elite_mating_rate
        self.mutation_rate = mutation_rate
        self.mutation_swap_rate = mutation_swap_rate

    def crossover(self, mom, dad):
        """ Ordered crossover """
        size = len(mom.vertices)

        # Choose random end/start position for crossover
        jane, jake = [-1] * size, [-1] * size
        start, end = sorted([random.randrange(size) for i in range(2)])

        # Replicate mom's sequence for jane, dad's sequence for jake
        for i in range(start, end +1):
            jane[i] = mom.vertices[i]
            jake[i] = dad.vertices[i]

        # Fill the remaining position with the other parent's entries
        current_dad_position, current_mom_position = 0, 0

        for i in chain(range(start), range(end+1, size)):

            while dad.vertices[current_dad_position] in jane:
                current_dad_position += 1

            while mom.vertices[current_mom_position] in jake:
                current_mom_position +=1

            jane[i] = dad.vertices[current_dad_position]
            jake[i] = mom.vertices[current_mom_position]

        return Tour(self.g, jane), Tour(self.g, jake)

    def mutate(self, tour):
        """ Randomly swaps pair of cities in agive tour according to mutation rate """

        # Decide whether to mutate
        if random.random() < self.mutation_rate:

            # for each vertex
            for i in range(len(tour.vertices)):

                # randomly decide whether to swap
                if random.random() < self.mutation_swap_rate:

                    # randomly choose other city
                    j = random.randrange(len(tour.vertices))

                    # Swap
                    tour.vertices[i], tour.vertices[j] = tour.vertices[j], tour.vertices[i]

    def select_parent(self, k):
        """" Implement k-tournament slection to choose parents """
        tournament = random.sample(self.population, k)
        return max(tournament, key=lambda t: t.cost())

    def evolve(self):
        """ Executes one iteration of the genetic algorithm to obtain new gen """

        new_population = []

        for _ in range(self.population_size):
            # K-tournament for parents
            mom, dad = self.select_parent(self.k), self.select_parent(self.k)
            jake, jane = self.crossover(mom, dad)

            # Mate in an elite fashion according to the elitism_rate
            if random.random() < self.elite_mating_rate:
                if jane.cost() < mom.cost() or jane.cost() < dad.cost():
                    new_population.append(jane)
                if jake.cost() < mom.cost() or jake.cost() < dad.cost():
                    new_population.append(jake)

            else:
                self.mutate(jane)
                self.mutate(jake)
                new_population += [jane, jake]

        # add new population to old
        self.population += new_population

        #Retain fittest
        self.population = heapq.nsmallest(self.population_size, self.population, key=lambda t: t.cost())

    def run(self, iterrations=5000):
        for _ in range(iterrations):
            self.evolve()

    def best(self):
        return max(self.population, key=lambda t: t.cost())
    